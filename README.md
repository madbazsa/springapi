### Run the project
1. Run the commands:
```
$ git clone git@gitlab.com:madbazsa/springapi.git
$ cd springapi
```
2. Set the database properties in the /src/main/resources/application.properties
3. Create a new database in MySql (name: springapi)
4. Test the app:
```
$ mvn test
$ mvn checkstyle:checkstyle
```
Open the /target/site/checkstyle.html file in the browser.
```
$ mvn spring-boot:run
```
Open a browser on [localhost:8080/employee/](http://localhost:8080/employee/)