package springapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;



import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import springapi.Application;
import springapi.domain.Employee;
import springapi.domain.Task;
import springapi.repository.TaskRepository;
import springapi.repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class EmployeeControllerTest {
    
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    private ObjectMapper mapper;

    public static final String BASE_TEST_URL = EmployeeController.BASE_URL + "/";

    private final List<Employee> employeeList = new ArrayList();

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.taskRepository.deleteAllInBatch();
        this.employeeRepository.deleteAllInBatch();

        Employee emp = new Employee(new Long(1), "testuser01", "(20)123-7654");
        emp.addTask(new Task("task 1"));
        emp.addTask(new Task("task 2"));
        this.employeeList.add(employeeRepository.save(emp));
        this.employeeList.add(employeeRepository.save(new Employee(new Long(2), "testuser02", "(70)834-2478")));
    }

    @Test
    public void createEmployee() throws Exception {

        Employee newEmployee = new Employee("testuser", "(30)475-1111");
        newEmployee.addTask(new Task("task 3"));
        newEmployee.addTask(new Task("task 4"));
 
        String empJson = mapper.writeValueAsString(newEmployee);

        this.mockMvc.perform(post(BASE_TEST_URL)
                .contentType(contentType)
                .content(empJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("testuser")))
                .andExpect(jsonPath("$.tasks[0].name", is("task 3")));
    }

    @Test
    public void createInvalidEmployee() throws Exception {
        String empJson = mapper.writeValueAsString(new Employee("t", "(20)334-6412"));

        this.mockMvc.perform(post(BASE_TEST_URL)
                .contentType(contentType)
                .content(empJson))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", is("ERROR")))
                .andExpect(jsonPath("$.message", is("The name should be between 3 and 20 characters")));
    }

    @Test
    public void employeeNotFound() throws Exception {
        mockMvc.perform(get(BASE_TEST_URL + "99999999")
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getOneEmployee() throws Exception {
        this.mockMvc.perform(get(BASE_TEST_URL + this.employeeList.get(0).getId().intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(this.employeeList.get(0).getName())))
                .andExpect(jsonPath("$.tasks", hasSize(2)));
    }

    @Test
    public void getAllEmployee() throws Exception {
        System.out.println(BASE_TEST_URL);
        this.mockMvc.perform(get(BASE_TEST_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].name", is(this.employeeList.get(1).getName())))
                .andExpect(jsonPath("$[1].id", is(this.employeeList.get(1).getId().intValue())))
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void updateEmployee() throws Exception {
        Employee emp = new Employee("updateduser", "(30)111-2234");
        emp.addTask(new Task("task 5"));
        String empJson = mapper.writeValueAsString(emp);
        
        this.mockMvc.perform(put(BASE_TEST_URL + this.employeeList.get(0).getId().intValue())
                .contentType(contentType)
                .content(empJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("updateduser")))
                .andExpect(jsonPath("$.id", is(this.employeeList.get(0).getId().intValue())))
                .andExpect(jsonPath("$.tasks[0].name", is("task 5")))
                .andExpect(jsonPath("$.tasks", hasSize(1)));
        assertEquals(taskRepository.count(), 1);
    }

    @Test
    public void updateInvalidEmployee() throws Exception {
        String empJson = mapper.writeValueAsString(new Employee("updateduser2", "(30)111-4"));
        
        this.mockMvc.perform(put(BASE_TEST_URL + this.employeeList.get(1).getId().intValue())
                .contentType(contentType)
                .content(empJson))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", is("ERROR")))
                .andExpect(jsonPath("$.message", is("The phone should be in this format: (00)000-0000")));
    }
    
    @Test
    public void deleteOneEmployeeWithoutTask() throws Exception {
        this.mockMvc.perform(delete(BASE_TEST_URL + this.employeeList.get(1).getId().intValue()))
                .andExpect(status().isNoContent());
        assertEquals(employeeRepository.count(), 1);
        assertEquals(taskRepository.count(), 2);
    }

    @Test
    public void deleteOneEmployeeWithTask() throws Exception {
        this.mockMvc.perform(delete(BASE_TEST_URL + this.employeeList.get(0).getId().intValue()))
                .andExpect(status().isNoContent());
        assertEquals(employeeRepository.count(), 1);
        assertEquals(taskRepository.count(), 0);
    }
}
