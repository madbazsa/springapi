package springapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import springapi.Application;
import springapi.domain.Employee;
import springapi.domain.Task;
import springapi.repository.TaskRepository;
import springapi.repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class TaskControllerTest {
    
    private MockMvc mockMvc;
 
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private ObjectMapper mapper;
    
    

    public static final String BASE_TEST_URL = TaskController.BASE_URL + "/";

    private final List<Task> taskList = new ArrayList();
    private final List<Employee> employeeList = new ArrayList();

    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.taskRepository.deleteAllInBatch();
        this.employeeRepository.deleteAllInBatch();

        Employee emp = new Employee(new Long(1), "testuser01", "(20)123-7654");
        Task t1 = taskRepository.save(new Task("task 1"));
        Task t2 = taskRepository.save(new Task("task 2"));
        emp.addTask(t1);
        emp.addTask(t2);
        this.taskList.add(t1);
        this.taskList.add(t2);
        
        this.employeeList.add(employeeRepository.save(emp));
        this.employeeList.add(employeeRepository.save(new Employee(new Long(2), "testuser02", "(70)834-2478")));
    }

    @Test
    public void createTask() throws Exception {
        Employee emp = this.employeeList.get(0);

        Task newTask = new Task("testtask");
        String taskJson = mapper.writeValueAsString(newTask);

        this.mockMvc.perform(post(BASE_TEST_URL + emp.getId())
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("testtask")));
    }

    @Test
    public void createInvalidTask() throws Exception {
        Employee emp = this.employeeList.get(0);
        Task newTask = new Task("t");
        String taskJson = mapper.writeValueAsString(newTask);

        this.mockMvc.perform(post(BASE_TEST_URL + emp.getId())
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", is("ERROR")))
                .andExpect(jsonPath("$.message", is("The task name should be between 3 and 20 characters")));
    }
    
    @Test
    public void createTaskInvalidEmployeeId() throws Exception {
        Task newTask = new Task("testtask");
        String taskJson = mapper.writeValueAsString(newTask);

        this.mockMvc.perform(post(BASE_TEST_URL + 999999999)
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isNotFound());
        assertEquals(taskRepository.count(), 2);
    }

    @Test
    public void taskNotFound() throws Exception {
        mockMvc.perform(get(BASE_TEST_URL + "99999999")
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getOneTask() throws Exception {
        this.mockMvc.perform(get(BASE_TEST_URL + this.taskList.get(1).getId().intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(this.taskList.get(1).getName())));
    }

    @Test
    public void getAllTask() throws Exception {
        this.mockMvc.perform(get(BASE_TEST_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].name", is(this.taskList.get(1).getName())))
                .andExpect(jsonPath("$[1].id", is(this.taskList.get(1).getId().intValue())))
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void updateAndMoveTask() throws Exception {
        String taskJson = mapper.writeValueAsString(new Task("updatedtask"));
        this.mockMvc.perform(put(BASE_TEST_URL
                                 + this.taskList.get(0).getId().intValue() + "/"
                                 + this.employeeList.get(1).getId().intValue())
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("updatedtask")))
                .andExpect(jsonPath("$.id", is(this.taskList.get(0).getId().intValue())));
        assertEquals(employeeRepository.findOne(this.employeeList.get(1).getId()).getTasks().get(0).getName(), "updatedtask");
        assertEquals(employeeRepository.findOne(this.employeeList.get(0).getId()).getTasks().size(), 1);
        assertEquals(employeeRepository.findOne(this.employeeList.get(1).getId()).getTasks().size(), 1);
    }
    
    @Test
    public void updateTask() throws Exception {
        String taskJson = mapper.writeValueAsString(new Task("updatedtask"));
        this.mockMvc.perform(put(BASE_TEST_URL
                                 + this.taskList.get(0).getId().intValue())
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("updatedtask")))
                .andExpect(jsonPath("$.id", is(this.taskList.get(0).getId().intValue())));
        assertEquals(employeeRepository.findOne(this.employeeList.get(0).getId()).getTasks().size(), 2);
        assertEquals(employeeRepository.findOne(this.employeeList.get(1).getId()).getTasks().size(), 0);
        assertEquals(employeeRepository.findOne(this.employeeList.get(0).getId()).getTasks().get(0).getName(), "updatedtask");
    }
    

    @Test
    public void updateInvalidTask() throws Exception {
        String taskJson = mapper.writeValueAsString(new Task("tt"));
        
        this.mockMvc.perform(put(BASE_TEST_URL + this.taskList.get(1).getId().intValue())
                .contentType(contentType)
                .content(taskJson))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type", is("ERROR")))
                .andExpect(jsonPath("$.message", is("The task name should be between 3 and 20 characters")));
    }
    
    @Test
    public void deleteTask() throws Exception {
        this.mockMvc.perform(delete(BASE_TEST_URL + this.taskList.get(0).getId().intValue()))
                .andExpect(status().isNoContent());
        assertEquals(taskRepository.count(), 1);
        assertNull(taskRepository.findOne(this.taskList.get(0).getId()));
    }
    
    @Test
    public void deleteTaskWithWrongTaskId() throws Exception {
        this.mockMvc.perform(delete(BASE_TEST_URL + 999999999))
                .andExpect(status().isNotFound());
        assertEquals(taskRepository.count(), 2);
    }
}
