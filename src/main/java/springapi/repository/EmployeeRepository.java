package springapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import springapi.domain.Employee;

/**
 * Employee repository.
 * @author madbazsa
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    /**
     * Get the employee who has this task.
     * @param id task id
     * @return employee
     */
    Employee findByTasksId(Long id);
}
