package springapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import springapi.domain.Task;

/**
 * Task repository.
 * @author madbazsa
 */
public interface TaskRepository extends JpaRepository<Task, Long> {

}
