package springapi.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import springapi.dto.MessageDTO;
import springapi.dto.MessageType;

/**
 * ControllerValidationHandler.
 * @author madbazsa
 */
@ControllerAdvice
public class ControllerValidationHandler {

    /**
     * MessageSource.
     */
    @Autowired
    private MessageSource msgSource;

    /**
     * processValidationError.
     * @param ex exception
     * @return field error
     */
    @ExceptionHandler(value = { MethodArgumentNotValidException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public final MessageDTO processValidationError(
                                    final MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        FieldError error = result.getFieldError();

        return processFieldError(error);
    }

    /**
     * processFieldError.
     * @param error FieldError
     * @return MessageDTO
     */
    private MessageDTO processFieldError(final FieldError error) {
        MessageDTO message = null;
        if (error != null) {
            Locale currentLocale = LocaleContextHolder.getLocale();
            String msg = msgSource.getMessage(error.getDefaultMessage(),
                                              null, currentLocale);
            message = new MessageDTO(MessageType.ERROR, msg);
        }
        return message;
    }
}
