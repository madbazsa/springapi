package springapi.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springapi.domain.Employee;
import springapi.service.EmployeeService;

/**
 * Employee Rest Controller.
 * @author madbazsa
 */
@RestController
public class EmployeeController {

    /**
     *  Base url for the Controller.
     */
    public static final String BASE_URL = "/employee";

    /**
     * Service.
     */
    private EmployeeService service;

    /**
     * Set service.
     * @param service service
     */
    @Autowired
    public final void setService(final EmployeeService service) {
            this.service = service;
    }

    /**
     * Get all the entities.
     * GET /employee
     * @return Json list.
     */
    @GetMapping(EmployeeController.BASE_URL)
    public final ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employees = service.findAll();
        if (employees.isEmpty()) {
            return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
    }

    /**
     * Get an entity.
     * GET /employee/{employeeId}
     * @param id id
     * @return json object
     */
    @GetMapping(EmployeeController.BASE_URL + "/{id}")
    public final ResponseEntity getEmployee(@PathVariable("id") final Long id) {
        Employee employee = service.findOne(id);
        if (employee == null) {
            return new ResponseEntity("Employee NOT found: "
                    + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(employee, HttpStatus.OK);
    }

    /**
     * Create an entity.
     * @param employee employee
     * @return The created entity
     */
    @PostMapping(EmployeeController.BASE_URL)
    public final ResponseEntity createEmployee(@RequestBody @Valid
                                                final Employee employee) {
        Employee result = service.save(employee);
        return new ResponseEntity(result, HttpStatus.CREATED);
    }

    /**
     * Delete an entity.
     * @param id id
     * @return httpStatus
     */
    @DeleteMapping(EmployeeController.BASE_URL + "/{id}")
    public final ResponseEntity deleteEmployee(@PathVariable final Long id) {
        Employee employee = service.findOne(id);
        if (employee == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Modify an entity.
     * @param id id
     * @param employee employee
     * @return the modified entity.
     */
    @PutMapping(EmployeeController.BASE_URL + "/{id}")
    public final ResponseEntity updateEmployee(
                                @PathVariable final Long id,
                                @Valid @RequestBody final Employee employee) {

        Employee result = service.update(id, employee);

        if (null == result) {
            return new ResponseEntity("Employee NOT found: " + id,
                                      HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
