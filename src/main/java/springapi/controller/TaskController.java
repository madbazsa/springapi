package springapi.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springapi.domain.Task;
import springapi.service.TaskService;

/**
 * Task Rest Controller.
 * @author madbazsa
 */
@RestController
public class TaskController {

    /**
     *  Base url for the Controller.
     */
    public static final String BASE_URL = "/task";

    /**
     * Service.
     */
    private TaskService service;

    /**
     * Set service.
     * @param service service
     */
    @Autowired
    public final void setService(final TaskService service) {
            this.service = service;
    }

    /**
     * Get all the entities.
     * GET /task
     * @return Json list.
     */
    @GetMapping(TaskController.BASE_URL)
    public final ResponseEntity<List<Task>> getTasks() {
        List<Task> tasks = service.findAll();
        if (tasks.isEmpty()) {
            return new ResponseEntity<List<Task>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
    }

    /**
     * Get an entity.
     * GET /task/{taskId}
     * @param id id
     * @return json object
     */
    @GetMapping(TaskController.BASE_URL + "/{id}")
    public final ResponseEntity getTask(@PathVariable("id") final Long id) {
        Task task = service.findOne(id);
        if (task == null) {
            return new ResponseEntity("Task NOT found: "
                    + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(task, HttpStatus.OK);
    }

    /**
     * Create an entity.
     * @param task task
     * @param empId ParentId
     * @return The created entity
     */
    @PostMapping(TaskController.BASE_URL + "/{empId}")
    public final ResponseEntity createTask(@RequestBody @Valid
                                            final Task task,
                                           @PathVariable("empId")
                                            final Long empId) {
        Task result = service.save(task, empId);
        if (result == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(result, HttpStatus.CREATED);
    }

    /**
     * Delete an entity.
     * @param id id
     * @return httpStatus
     */
    @DeleteMapping(TaskController.BASE_URL + "/{id}")
    public final ResponseEntity deleteTask(@PathVariable final Long id) {
        Task task = service.findOne(id);
        if (task == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Modify an entity.
     * @param id id
     * @param moveToEmpId Move the task to another Employee
     * @param task task
     * @return the modified entity.
     */
    @PutMapping(value = {TaskController.BASE_URL + "/{id}",
                         TaskController.BASE_URL + "/{id}/{moveToEmpId}"})
    public final ResponseEntity updateTask(
                                @PathVariable final Long id,
                                @PathVariable final Optional<Long> moveToEmpId,
                                @Valid @RequestBody final Task task) {
        Long moveTo = moveToEmpId.isPresent() ? moveToEmpId.get() : null;
        Task result = service.update(id, moveTo, task);

        if (null == result) {
            return new ResponseEntity("Task NOT found: " + id,
                                      HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
