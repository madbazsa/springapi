package springapi.dto;

/**
 * MessageType.
 * @author madbazsa
 */
public enum MessageType {

    /**
     * SUCCESS MessageType.
     */
    SUCCESS,

    /**
     * INFO MessageType.
     */
    INFO,

    /**
     * WARNING MessageType.
     */
    WARNING,

    /**
     * ERROR MessageType.
     */
    ERROR
}
