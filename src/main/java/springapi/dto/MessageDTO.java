package springapi.dto;

/**
 * MessageDTO.
 * @author madbazsa
 */
public class MessageDTO {

    /**
     * message text.
     */
    private String message;

    /**
     * Message type.
     */
    private MessageType type;

    /**
     * Constructor for MessageDTO.
     */
    public MessageDTO() {
        super();
    }

    /**
     * Constructor with parameters for MessageDTO.
     * @param type type
     * @param message message
     */
    public MessageDTO(final MessageType type, final String message) {
        super();
        this.message = message;
        this.type = type;
    }

    /**
     * Get message.
     * @return message
     */
    public final String getMessage() {
        return message;
    }

    /**
     * Set message.
     * @param message message
     */
    public final void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Get type.
     * @return type
     */
    public final MessageType getType() {
        return type;
    }

    /**
     * Set type.
     * @param type type
     */
    public final void setType(final MessageType type) {
        this.type = type;
    }
}
