package springapi.service;

import java.util.List;

import springapi.domain.Employee;

/**
 * EmployeeServiceImpl.
 * @author madbazsa
 */
public interface EmployeeService {

    /**
     * Get an entity by id.
     * @param id id
     * @return entity
     */
    Employee findOne(Long id);

    /**
     * Save an entity.
     * @param employee employee
     * @return created entity
     */
    Employee save(Employee employee);

    /**
     * Update an entity.
     * @param id id
     * @param employee employee
     * @return modified employee
     */
    Employee update(Long id, Employee employee);

    /**
     * Delete an entity by id.
     * @param id id
     * @return hasDeleted
     */
    Boolean delete(Long id);

    /**
     * Get all entities.
     * @return employee list
     */
    List<Employee> findAll();
}
