package springapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springapi.domain.Employee;

import springapi.domain.Task;
import springapi.repository.TaskRepository;
import springapi.repository.EmployeeRepository;
import springapi.service.TaskService;

/**
 * TaskServiceImpl.
 * @author madbazsa
 */
@Service
public class TaskServiceImpl implements TaskService {

    /**
     * Employee Repository.
     */
    private final EmployeeRepository employeeRepository;

    /**
     * Task Repository.
     */
    private final TaskRepository taskRepository;

    /**
     * Constructor.
     * @param employeeRepository employeeRepository
     * @param taskRepository taskRepository
     */
    @Autowired
    public TaskServiceImpl(final EmployeeRepository employeeRepository,
                           final TaskRepository taskRepository) {
        this.employeeRepository = employeeRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Get an entity by id.
     * @param id id
     * @return entity
     */
    @Override
    public final Task findOne(final Long id) {
        return taskRepository.findOne(id);
    }

    /**
     * Save an entity.
     * @param task task
     * @param empId ParentId
     * @return created entity
     */
    @Override
    public final Task save(final Task task, final Long empId) {
        Employee emp = employeeRepository.findOne(empId);
        if (emp == null) {
            return null;
        }
        emp.addTask(task);
        employeeRepository.save(emp);
        return task;
    }

    /**
     * Update an entity.
     * @param id id
     * @param moveToEmpId Move the task to another Employee
     * @param task task
     * @return modified task
     */
    @Override
    public final Task update(final Long id,
                             final Long moveToEmpId,
                             final Task task) {
        Task modTask = taskRepository.findOne(id);
        modTask.setName(task.getName());
        if (moveToEmpId != null) {
            Employee empTo = employeeRepository.findOne(moveToEmpId);
            if (empTo != null) {
                Employee empFrom = employeeRepository.findByTasksId(id);
                empFrom.removeTask(modTask);
                employeeRepository.save(empFrom);
                empTo.addTask(modTask);
                employeeRepository.save(empTo);
            }
        }
        taskRepository.flush();
        return modTask;
    }

    /**
     * Delete an entity by id.
     * @param id id
     */
    @Override
    public final void delete(final Long id) {
        Employee empDelFrom = employeeRepository.findByTasksId(id);
        Task delTask = taskRepository.findOne(id);
        empDelFrom.removeTask(delTask);
        employeeRepository.save(empDelFrom);
    }

    /**
     * Get all entities.
     * @return task list
     */
    @Override
    public final List<Task> findAll() {
        return taskRepository.findAll();
    }
}
