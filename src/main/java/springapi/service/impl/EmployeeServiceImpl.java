package springapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springapi.domain.Employee;
import springapi.domain.Task;
import springapi.repository.TaskRepository;
import springapi.repository.EmployeeRepository;
import springapi.service.EmployeeService;

/**
 * EmployeeServiceImpl.
 * @author madbazsa
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    /**
     * Repository.
     */
    private final EmployeeRepository employeeRepository;

    /**
     * Repository.
     */
    private final TaskRepository taskRepository;

    /**
     * Constructor.
     * @param employeeRepository employeeRepository
     * @param taskRepository taskRepository
     */
    @Autowired
    public EmployeeServiceImpl(final EmployeeRepository employeeRepository,
                           final TaskRepository taskRepository) {
        this.employeeRepository = employeeRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Get an entity by id.
     * @param id id
     * @return entity
     */
    @Override
    public final Employee findOne(final Long id) {
        return employeeRepository.findOne(id);
    }

    /**
     * Save an entity.
     * @param employee employee
     * @return created entity
     */
    @Override
    public final Employee save(final Employee employee) {
        Employee e = new Employee(employee.getName(), employee.getPhone());
        for (Task t : employee.getTasks()) {
            e.addTask(new Task(t.getName()));
        }
        return employeeRepository.save(e);
    }

    /**
     * Update an entity.
     * @param employee employee
     * @param id id
     * @return modified employee
     */
    @Override
    public final Employee update(final Long id, final Employee employee) {
        Employee old = employeeRepository.findOne(id);
        if (old == null) {
            return null;
        }
        old.setName(employee.getName());
        old.setPhone(employee.getPhone());
        for (Task t : old.getTasks()) {
            taskRepository.delete(t.getId());
        }
        old.getTasks().clear();
        for (Task t : employee.getTasks()) {
            old.addTask(new Task(t.getName()));
        }
        employeeRepository.flush();
        return old;
    }

    /**
     * Delete an entity by id.
     * @param id id
     * @return hasDeleted
     */
    @Override
    public final Boolean delete(final Long id) {
        try {
            Employee delEmp = employeeRepository.findOne(id);
            for (Task t : delEmp.getTasks()) {
                taskRepository.delete(t.getId());
            }
            employeeRepository.delete(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Get all entities.
     * @return employee list
     */
    @Override
    public final List<Employee> findAll() {
        return employeeRepository.findAll();
    }
}
