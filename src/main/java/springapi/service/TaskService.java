package springapi.service;

import java.util.List;

import springapi.domain.Task;

/**
 * TaskServiceImpl.
 * @author madbazsa
 */
public interface TaskService {

    /**
     * Get an entity by id.
     * @param id id
     * @return entity
     */
    Task findOne(Long id);

    /**
     * Save an entity.
     * @param task task
     * @param empId ParentId
     * @return created entity
     */
    Task save(Task task, Long empId);

    /**
     * Update an entity.
     * @param id id
     * @param moveToEmpId Move the task to another Employee
     * @param task task
     * @return modified task
     */
    Task update(Long id, Long moveToEmpId, Task task);

    /**
     * Delete an entity by id.
     * @param id id
     */
    void delete(Long id);

    /**
     * Get all entities.
     * @return task list
     */
    List<Task> findAll();
}
