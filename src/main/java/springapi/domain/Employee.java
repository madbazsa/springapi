package springapi.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Employee entity.
 * @author madbazsa
 */
@Entity
public class Employee {

    /**
     * Employee's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Employee's name.
     */
    @NotNull(message = "error.require")
    @Size(min = 3, max = 20, message = "error.employee.name.size")
    private String name;

    /**
     * Employee's phone.
     */
    @NotNull(message = "error.require")
    @Pattern(regexp = "\\(\\d{2}\\)\\d{3}-\\d{4}",
             message = "error.employee.phone.format")
    private String phone;

    /**
     * Employee's tasks.
     */
    @OneToMany(
        fetch = FetchType.EAGER,
        mappedBy = "employee",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList();

    /**
     * Get id.
     * @return id
     */
    public final Long getId() {
        return id;
    }

    /**
     * Set id.
     * @param id id
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Get name.
     * @return name
     */
    public final String getName() {
        return name;
    }

    /**
     *  Set name.
     * @param name name
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * Get Phone.
     * @return phone
     */
    public final String getPhone() {
        return phone;
    }

    /**
     * Set Phone.
     * @param phone phone
     */
    public final void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * Get tasks.
     * @return tasks
     */
    public final List<Task> getTasks() {
        return tasks;
    }

    /**
     * Set tasks.
     * @param tasks tasks
     */
    public final void setTasks(final List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Add a task to this employee.
     * @param task task
     */
    public final void addTask(final Task task) {
        tasks.add(task);
        task.setEmployee(this);
    }

    /**
     * Remove a task from this employee.
     * @param task task
     */
    public void removeTask(final Task task) {
        tasks.remove(task);
        task.setEmployee(null);
    }

    /**
     * Employee constructor with all parameters.
     * @param id id
     * @param name name
     * @param phone phone
     * @param tasks tasks
     */
    public Employee(final Long id,
                    final String name,
                    final String phone,
                    final List<Task> tasks) {
        super();
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.tasks = tasks;
    }

    /**
     * Employee constructor without id parameter.
     * @param name name
     * @param phone phone
     * @param tasks tasks
     */
    public Employee(final String name,
                    final String phone,
                    final List<Task> tasks) {
        super();
        this.name = name;
        this.phone = phone;
        this.tasks = tasks;
    }

    /**
     * Employee constructor.
     * @param id id
     * @param name name
     * @param phone phone
     */
    public Employee(final Long id,
                    final String name,
                    final String phone) {
        super();
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    /**
     * Employee constructor.
     * @param name name
     * @param phone phone
     */
    public Employee(final String name,
                    final String phone) {
        super();
        this.name = name;
        this.phone = phone;
    }

    /**
     * Employee constructor without parameters.
     */
    public Employee() {
        super();
    }
}
