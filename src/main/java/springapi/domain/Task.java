package springapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Task entity.
 * @author madbazsa
 */
@Entity
public class Task {

    /**
     * Task id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Task name.
     */
    @NotNull(message = "error.require")
    @Size(min = 3, max = 20, message = "error.task.name.size")
    private String name;

    /**
     * Employee.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    @JsonIgnore
    private Employee employee;

    /**
     *  Get id.
     * @return id
     */
    public final Long getId() {
        return id;
    }

    /**
     * Set id.
     * @param id id
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Get name.
     * @return name
     */
    public final String getName() {
        return name;
    }

    /**
     *  Set name.
     * @param name name
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * Get employee.
     * @return employee
     */
    public final Employee getEmployee() {
        return employee;
    }

    /**
     * Set employee.
     * @param employee employee
     */
    public final void setEmployee(final Employee employee) {
        //employee.getTasks().add(this);
        this.employee = employee;
    }

    /**
     * Task constructor with all parameters.
     * @param id id
     * @param name name
     * @param employee employee
     */
    public Task(final Long id, final String name, final Employee employee) {
        super();
        this.id = id;
        this.name = name;
        this.employee = employee;
    }

    /**
     * Task constructor without id parameter.
     * @param name name
     * @param employee employee
     */
    public Task(final String name, final Employee employee) {
        super();
        this.name = name;
        this.employee = employee;
    }

    /**
     * Task constructor with all parameters.
     * @param id id
     * @param name name
     */
    public Task(final Long id, final String name) {
        super();
        this.id = id;
        this.name = name;
    }

    /**
     * Task constructor.
     * @param name name
     */
    public Task(final String name) {
        super();
        this.name = name;
    }

    /**
     * Task constructor without parameters.
     */
    public Task() {
        super();
    }

    /**
     * Are equal tasks?
     * @param o task
     * @return boolean
     */
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        return id != null && id.equals(((Task) o).id);
    }

    /**
     * hashCode.
     * @return 31
     */
    @Override
    public final int hashCode() {
        return 31;
    }
}
